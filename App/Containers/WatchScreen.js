import React, { Component } from 'react'
import { View, Text, KeyboardAvoidingView, SectionList, ActivityIndicator } from 'react-native';
import { LineChart, YAxis, Grid } from 'react-native-svg-charts'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'
import Selector from '../Components/Selector';

// Styles
import styles from './Styles/WatchScreenStyle'

class WatchScreen extends Component {

  componentDidMount(){
    if(this.props.option){
      this.props.selectOption(this.props.option)
    }
  }

  renderChart(){
    if(!this.props.loading && this.props.option && this.props.volumes.length > 0){
      const contentInset = { top: 20, bottom: 20 }

      const data = this.props.volumes.reverse();

      return (
          <View style={{ height: 200, flexDirection: 'row' }}>
              <YAxis
                  data={ data }
                  contentInset={ contentInset }
                  svg={{
                      fill: 'grey',
                      fontSize: 10,
                  }}
                  numberOfTicks={ 10 }
                  formatLabel={ value => `${value}` }
              />
              <LineChart
                  style={{ flex: 1, marginLeft: 16 }}
                  data={ data}
                  svg={{ stroke: 'rgb(134, 65, 244)' }}
                  contentInset={ contentInset }
              >
                  <Grid/>
              </LineChart>
          </View>
      )
    }
    
  }

  renderList(){
    if(!this.props.loading && this.props.option){
      console.log(this.props.data);
      return(
        <SectionList
          style={{flex: 1}}
          renderItem={({item, index, section}) => (
            <View style={styles.tableItemContainer} key={index}>
              <Text style={styles.tableItem}>{item["date"]}</Text>
              <Text style={styles.tableItem}>{item["4. close"]}</Text>
              <Text style={styles.tableItem}>{item["5. volume"]}</Text>
            </View>
          )}
          renderSectionHeader={({section: {title}}) => (
            <View style={styles.tableHeaderContainer}>
              <Text style={styles.tableHeaderItem}>Date</Text>
              <Text style={styles.tableHeaderItem}>Close</Text>
              <Text style={styles.tableHeaderItem}>Volume</Text>
            </View>
            
          )}
          sections={[
            {title: 'Data', data: this.props.data}
          ]}
          keyExtractor={(item, index) => item + index}
        />
      );
    } 
    
  }

  renderIndicator(){
    if(this.props.loading){
      return(
        <ActivityIndicator size="large" style={{flex: 1}} />
      );
    }
  }
  
  render () {
    return (
      <View style={styles.container}>
        <Selector style={{margin: 30}} label="Options" selected={this.props.option} options={this.props.options} onSelect={(item) => {
            this.props.selectOption(item);
          }}/>
        {this.renderIndicator()}
        {this.renderChart()}
        {this.renderList()}
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  console.log(state);
  return {
    loading: state.watch.loading,
    options: state.watch.options,
    option: state.watch.option,
    data: state.watch.data,
    volumes: state.watch.volumes
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    selectOption: (option) => {
      if(option){
        dispatch({ type: "REQUEST_CHANGE_OPTION", option})
      }
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(WatchScreen)
