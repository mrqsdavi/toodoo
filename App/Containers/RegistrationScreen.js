import React, { Component } from 'react'
import { View, ScrollView, Text, KeyboardAvoidingView, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'
import Input from '../Components/Input';
import Selector from '../Components/Selector';
import { CheckBox } from 'react-native-elements'

//Saga
import { changeCountry } from '../Sagas/RegistrationSaga';
// Styles
import styles from './Styles/RegistrationScreenStyle'

class RegistrationScreen extends Component {

  constructor(props){
    super(props);

    this.state = {
      jointAccount: false
    }
  }

  renderJointAccount(){
    if(this.state.jointAccount){
      return(
        <View>
          <Input label="Spouse Name"/>
          <Input label="Spouse DoB"/>
        </View>
      );
    }
  }

  renderCheck(){
    return (
      <View style={styles.checkContainer}>
        <Text style={styles.label}>Joint Account</Text>
        <CheckBox
              center
              containerStyle={{backgroundColor: 'transparent', borderWidth: 0}}
              title='Yes'
              checkedIcon='dot-circle-o'
              uncheckedIcon='circle-o'
              onPress={() => this.setState({jointAccount: true})}
              checked={this.state.jointAccount}
            />
        <CheckBox
            center
            containerStyle={{backgroundColor: 'transparent', borderWidth: 0}}
            title='No'
            checkedIcon='dot-circle-o'
            uncheckedIcon='circle-o'
            onPress={() => this.setState({jointAccount: false})}
            checked={!this.state.jointAccount}
          />
      </View>
    );
  }

  render () {
    return (
      <ScrollView style={styles.container}>
        <KeyboardAvoidingView>
          <Input label="Name"/>
          <Input label="Date of Birth"/>
          <Input label="Net Worth"/>
          <Input label="Address"/>
          <Selector label="Country" selected={this.props.country} options={this.props.countries} onSelect={(item) => {
            this.props.selectCountry(item);
          }}/>
          <Selector label="State" selected={this.props.state} options={this.props.states} enabled={this.props.country != null} onSelect={(item) => {
            this.props.selectState(item);
          }}/>
          <Selector label="City" selected={this.props.city} options={this.props.cities} enabled={this.props.country != null && this.props.state != null} onSelect={(item) => {
            this.props.selectCity(item);
          }}/>
          {this.renderCheck()}
          {this.renderJointAccount()}
          <TouchableOpacity style={styles.button}>
            <Text style={styles.buttonText}>Submit</Text>
          </TouchableOpacity>
        </KeyboardAvoidingView>
      </ScrollView>
    )
  }
}

const mapStateToProps = (state) => {
  console.log(state);
  return {
    countries: state.registration.countries,
    country: state.registration.country,
    states: state.registration.states,
    state: state.registration.state,
    cities: state.registration.cities,
    city: state.registration.city,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    selectCountry: (country) => {
      const action = {type: "REQUEST_CHANGE_COUNTRY", country};
      dispatch(action);
    },
    selectState: (state) => {
      const action = {type: "REQUEST_CHANGE_STATE", state};
      dispatch(action);
    },
    selectCity: (city) => {
      const action = {type: "REQUEST_CHANGE_CITY", city};
      dispatch(action);
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RegistrationScreen)
