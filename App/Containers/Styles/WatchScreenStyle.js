import { StyleSheet } from 'react-native'
import { ApplicationStyles, Colors, Fonts } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container:{
    flex: 1,
    // padding: 30,
    backgroundColor: Colors.background
  },
  tableHeaderContainer:{
    width: "100%",
    height: 40,
    backgroundColor: Colors.panther,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  tableHeaderItem:{
    ...Fonts.style.bold,
    flex: 1,
    height: 40,
    lineHeight:35,
    justifyContent: 'center', 
    alignItems: 'center',
    textAlign: 'center',
    borderWidth: 1,
    borderColor: Colors.silver,
    color: Colors.silver
  },
  tableItemContainer:{
    width: "100%",
    height: 40,
    backgroundColor: Colors.background,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  tableItem:{
    ...Fonts.style.small,
    flex: 1,
    height: 40,
    lineHeight:35,
    justifyContent: 'center', 
    alignItems: 'center',
    textAlign: 'center',
    borderWidth: 1,
    borderColor: Colors.panther
  }

})
