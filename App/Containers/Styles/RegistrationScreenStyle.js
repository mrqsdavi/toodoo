import { StyleSheet } from 'react-native'
import { ApplicationStyles, Colors, Fonts } from '../../Themes/';

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    flex: 1,
    padding: 30,
    backgroundColor: Colors.background
  },
  checkContainer: {
    flex: 1,
    marginBottom: 5,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    overflow: 'hidden'
  },
  label: {
    ...Fonts.style.normal,
    flex: 1,
    paddingRight: 10,
    textAlign: 'right'
  },
  optionsContainer: {
    flex: 2,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: Colors.frost,
    backgroundColor: Colors.steel
  },
  button: {
    flex: 1,
    height: 55,
    borderRadius: 10,
    marginTop: 50,
    backgroundColor:  Colors.steel,
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonText: {
    ...Fonts.style.bold,
    fontSize: Fonts.size.h5
  }
})
