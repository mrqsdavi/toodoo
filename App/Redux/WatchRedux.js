import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
    changeOption: ['country'],
    dataLoaded: ['data']
})
  
export const WatchTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
    loading: false,
    options: ["MSFT", "AAPL", "AMZN"],
    option: "",
    data: [],
    volumes: []
});

export const option = (state, { option }) => {
    if(option == null){
        return { ... state, loading: false, option };
    }
    return { ... state, loading: true, option };
}

export const dataLoaded = (state, { data, volumes }) => {
    return { ... state, loading: false, data, volumes };
}

export const reducer = createReducer(INITIAL_STATE, {
    [Types.CHANGE_OPTION]: option,
    [Types.DATA_LOADED]: dataLoaded
})

  