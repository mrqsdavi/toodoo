import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
    changeCountry: ['country'],
    changeState: ['state'],
    changeCity: ['city']
})
  
export const RegistrationTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
    countries: ["Brazil", "USA"],
    country: null,
    states: [],
    state: null,
    cities: [],
    city: null
});

export const country = (state, { country }) => {
    if(country == "Brazil"){
        return { ...state, country: country, states: ["Alagoas", "São Paulo", "Sergipe"] };
    }else if(country == "USA"){
        return { ...state, country: country, states: ["Florida", "California", "Nebraska"] };
    }

    return { ... state, country: country};
}

export const state = (state, action) => {
    if(action.state == "Alagoas"){
        return { ...state, state: action.state, cities: ["Maceió", "Arapiraca"] };
    }else if(action.state == "São Paulo"){
        return { ...state, state: action.state, cities: ["São Paulo", "Campinas", "Paulínia"] };
    }else if(action.state == "Sergipe"){
        return { ...state, state: action.state, cities: ["Aracaju", "Itabaiana", "Estância"] };
    }else if(action.state == "Florida"){
        return { ...state, state: action.state, cities: ["Miami", "Orlando", "Tampa"] };
    }else if(action.state == "California"){
        return { ...state, state: action.state, cities: ["Los Angeles", "San Francisco", "San Diego", "Sacramento"] };
    }else if(action.state == "Nebraska"){
        return { ...state, state: action.state, cities: ["Omaha", "Lincoln"] };
    }

    return { ...state, state: action.state, cities: []};
}

export const city = (state, { city }) => {
    return { ...state, city};
}

export const reducer = createReducer(INITIAL_STATE, {
    [Types.CHANGE_COUNTRY]: country,
    [Types.CHANGE_STATE]: state,
    [Types.CHANGE_CITY]: city
})

  