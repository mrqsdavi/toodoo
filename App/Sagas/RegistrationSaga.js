import { call, put } from 'redux-saga/effects'
import { path } from 'ramda'
import RegistrationActions from '../Redux/RegistrationRedux';
const Types = require('../Redux/RegistrationRedux').RegistrationTypes;

export function * changeCountry (action) {
    const { country } = action;
    yield put({ type: Types.CHANGE_COUNTRY, country: country});
}

export function * changeState (action) {
    const { state } = action;
    yield put({ type: Types.CHANGE_STATE, state: state});
}

export function * changeCity (action) {
    const { city } = action;
    yield put({ type: Types.CHANGE_CITY, city: city});
}