import { takeLatest, takeEvery, all } from 'redux-saga/effects'
import API from '../Services/Api'
import FixtureAPI from '../Services/FixtureApi'
import DebugConfig from '../Config/DebugConfig'
import AlphaVantageApi from '../Services/AlphaVantageApi'

/* ------------- Types ------------- */

import { StartupTypes } from '../Redux/StartupRedux'
import { GithubTypes } from '../Redux/GithubRedux'
import { RegistrationTypes } from '../Redux/RegistrationRedux'

/* ------------- Sagas ------------- */
import { startup } from './StartupSagas'
import { getUserAvatar } from './GithubSagas'
import { changeCountry, changeState, changeCity } from './RegistrationSaga'
import { changeOption } from './WatchSagas'

/* ------------- API ------------- */

// The API we use is only used from Sagas, so we create it here and pass along
// to the sagas which need it.
const api = DebugConfig.useFixtures ? FixtureAPI : API.create()

/* ------------- Connect Types To Sagas ------------- */

export default function * root () {
  yield all([
    // some sagas only receive an action
    takeLatest(StartupTypes.STARTUP, startup),
    takeLatest("REQUEST_CHANGE_COUNTRY", changeCountry),
    takeLatest("REQUEST_CHANGE_STATE", changeState),
    takeLatest("REQUEST_CHANGE_CITY", changeCity),
    takeLatest("REQUEST_CHANGE_OPTION", changeOption, AlphaVantageApi.create()),

    // some sagas receive extra parameters in addition to an action
    takeLatest(GithubTypes.USER_REQUEST, getUserAvatar, api),
  ])
}
