import { call, put } from 'redux-saga/effects'
import { path } from 'ramda'
const Types = require('../Redux/WatchRedux').WatchTypes;
import moment from 'moment';

export function * changeOption (api, action) {
    const { option } = action;
    yield put({ type: Types.CHANGE_OPTION, option: option});
    const response = yield call(api.getTable, option);
    console.log(response);
    var obj = response.data["Time Series (1min)"];
    var volumes = [];
    var data = Object.keys(obj).map(function (key) {
        var o = obj[key];
        o["date"] = moment(key,"YYYY-MM-DD hh:mm").format("DD/MM/YYYY hh:mm");
        volumes.push(Number(o["5. volume"]))
        return o; 
    });
    yield put({ type: "DATA_LOADED", data, volumes})
}