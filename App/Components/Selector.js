import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { View, Animated, Text, TouchableOpacity, Image } from 'react-native'
import styles from './Styles/SelectorStyle'
import { Images } from '../Themes';

const ITEM_HEIGHT = 35;

export default class Selector extends Component {
  // // Prop type warnings
  static propTypes = {
    selected: PropTypes.string,
    options: PropTypes.array.isRequired,
    onSelect: PropTypes.func,
    enabled: PropTypes.bool
  }

  static defaultProps = {
    enabled: true,
    selected: '',
    onSelect: () => {}
  }

  constructor(props) {
    super(props);

    this.state = {
      opened: false
    }
    this.height = new Animated.Value(ITEM_HEIGHT);
  }

  onPress(item){
    const { options } = this.props;

    if(this.state.opened){
      this.props.onSelect(item);
      Animated.spring(this.height, {
        toValue: ITEM_HEIGHT
      }).start();
    }else{
      Animated.spring(this.height, {
        toValue: ITEM_HEIGHT * (options.length+1)
      }).start();
    }

    this.setState({ opened: !this.state.opened });
    
  }

  renderOptions(){
    const { options } = this.props;
    
    if(options){
      const indexSelected = options.indexOf(this.props.selected);
      return options.map((item, index) => {
        return (
          <View key={index}>
          <TouchableOpacity 
            onPress={() => this.onPress(item)}
            style={styles.option}>
            <Text style={[indexSelected == index ? styles.selected : styles.normal]}>{item}</Text>
          </TouchableOpacity>
          <View style={styles.divider}/>
          </View>
        );
      });
    }
  }

  render () {

    const { options } = this.props;
    if(!options){
      return (<View />);
    }
    const indexSelected = options.indexOf(this.props.selected);

    const opacityStyle = {
      opacity: this.props.enabled ? 1 : 0.3
    }

    const pointerEvents = this.props.enabled ? "auto" : "none";

    const viewStyle = {
      height: this.height,
      // transform: [
      //   { translateY: this.state.opened ? 0 : -ITEM_HEIGHT * (indexSelected+1)}
      // ]
    };

    // alert(-ITEM_HEIGHT * (indexSelected+1));

    const downIconInterpolate = this.height.interpolate({
      inputRange: [ITEM_HEIGHT, ITEM_HEIGHT*(options.length+1)],
      outputRange: ["0deg", "180deg"]
    });

    const downIconStyle = {
      transform: [
        { rotateZ: downIconInterpolate }
      ]
    }

    

    return (
      <View style={[styles.container, this.props.style]}>
        <Text style={styles.label}>{this.props.label}</Text>
        <View style={[styles.optionsContainer, opacityStyle]} pointerEvents={pointerEvents}>
          <Animated.View style={viewStyle}>
          <TouchableOpacity 
            onPress={() => this.onPress(null)}
            style={styles.option}>
            <Text style={[indexSelected < 0 || !this.state.opened ? styles.selected : styles.normal]}>{ this.state.opened || indexSelected < 0 ? "Select an option..." : this.props.selected}</Text>
          </TouchableOpacity>
          <View style={styles.divider}/>
            {this.renderOptions()}
          </Animated.View>
          <Animated.View style={[styles.downContainer, downIconStyle]}>
            <Image source={Images.down} style={styles.downIcon} />
          </Animated.View>
        </View>
      </View>
    )
  }
}
