import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { View, Text, TextInput } from 'react-native'
import styles from './Styles/InputStyle'

export default class Input extends Component {
  // // Prop type warnings
  static propTypes = {
    label: PropTypes.string.isRequired
  }
  //
  // // Defaults for props
  // static defaultProps = {
  //   someSetting: false
  // }

  render () {
    return (
      <View style={styles.container}>
        <Text style={styles.label}>{this.props.label}</Text>
        <View style={styles.inputContainer}>
          <TextInput style={styles.input} underlineColorAndroid="transparent" />
        </View>
      </View>
    )
  }
}
