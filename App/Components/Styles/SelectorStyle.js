import { StyleSheet } from 'react-native';
import { Fonts, Colors } from '../../Themes';

export default StyleSheet.create({
  container: {
    marginBottom: 5,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    overflow: 'hidden'
  },
  divider: {
    width: '100%',
    height: 1,
    backgroundColor: Colors.frost
  },
  label: {
    ...Fonts.style.normal,
    flex: 1,
    paddingRight: 10,
    textAlign: 'right'
  },
  optionsContainer: {
    flex: 2,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: Colors.frost,
    backgroundColor: Colors.steel,
    overflow: 'hidden'
  },
  option: {
    height: 35,
    paddingLeft: 5,
    paddingRight: 5,
    justifyContent: 'center'
  },
  normal: {
    ...Fonts.style.normal
  },
  selected: {
    ...Fonts.style.bold
  },
  downContainer: {
    position: 'absolute',
    top: 14,
    right: 10
  },
  downIcon: {
    tintColor: Colors.black,
    width: 10,
    height: 7
  }
})
