import { StyleSheet } from 'react-native';
import { Fonts, Colors } from '../../Themes';

export default StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 5
  },
  label: {
    ...Fonts.style.normal,
    flex: 1,
    paddingRight: 10,
    textAlign: 'right'
  },
  inputContainer: {
    flex: 2,
    height:  35,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: Colors.frost,
    backgroundColor: Colors.steel
  },
  input: {
    ...Fonts.style.bold,
    flex: 1,
    height:  35,
    paddingLeft: 5,
    paddingRight: 5,
    paddingTop: 0,
    paddingBottom: 0,
  }
})
