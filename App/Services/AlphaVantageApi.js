import apisauce from 'apisauce'

const create = (baseURL = 'https://www.alphavantage.co/') => {

  // const apiKey = 'AGVBTCH1EK49PD4R';
  const apiKey = 'demo'

  const api = apisauce.create({
    // base URL is read from the "constructor"
    baseURL,
    // here are some default headers
    headers: {
      'Cache-Control': 'no-cache'
    },
    // 10 second timeout...
    timeout: 10000
  });

  getQuery = (table) => {
      return `query?function=TIME_SERIES_INTRADAY&symbol=${table}&interval=1min&apikey=${apiKey}`;
  }

  
  const getTable = (table) => api.get(getQuery(table))

  
  return {
    getTable
  }
}

export default {
  create
}
