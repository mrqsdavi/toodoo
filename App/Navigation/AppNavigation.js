import { StackNavigator, TabNavigator } from 'react-navigation'
import { Platform} from 'react-native';
import WatchScreen from '../Containers/WatchScreen'
import RegistrationScreen from '../Containers/RegistrationScreen'
// import LaunchScreen from '../Containers/LaunchScreen'
import { Colors } from '../Themes';

import styles from './Styles/NavigationStyles'

const tab = TabNavigator({
  WatchScreen: { screen: WatchScreen },
  RegistrationScreen: { screen: RegistrationScreen },
},{
  tabBarOptions : {
    style: {
      backgroundColor: Colors.panther,
    },
    activeTintColor: Colors.background,
    indicatorStyle: {
      backgroundColor: 'tranparent',
    },
    labelStyle: { 
      fontSize: 13,
      marginBottom: Platform.OS == 'ios' ? 15 : 5
     },
    showIcon: false
  },
  tabBarPosition: 'bottom',
});

// Manifest of possible screens
const PrimaryNav = StackNavigator({
  TabScreen: tab
  // LaunchScreen: { screen: LaunchScreen }
}, {
  // Default config for all screens
  headerMode: 'none',
  initialRouteName: 'TabScreen',
  navigationOptions: {
    headerStyle: styles.header
  }
})

export default PrimaryNav
